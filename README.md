SIMPO project
====================

SIMPO is a Web application for planning and  forecasting of rescue operations. This tool support rescue operations.

## Installation ##

Before deploing SIMPO you need install the next software:

*   [Apache HTTP Server 2.0, 2.2 or 2.4](https://httpd.apache.org/download.cgi#apache24) or httpd (RHEL, CentOS or Fedora)

    Also need install apache2-dev ([Debian](https://packages.debian.org/ru/sid/apache2-dev)) or httpd-devel (RHEL, CentOS or Fedora)

*   [PostgreSQL 9.1 and higher](https://www.postgresql.org/download/)
*   [Python 3.6](https://www.python.org/downloads/release/python-361/)

    Also for Python you must install:

    -   python3-dev ([Debian](https://packages.debian.org/ru/sid/python3-dev))
    -   [Pip3](https://pip.pypa.io/en/stable/installing/) (optional) (Debian python3-pip)
    -   libpq-dev to compile psycopg2 on Debian 
    -   psycopg2 from [source code](http://initd.org/psycopg/docs/install.html#install-from-source) or from [pip](http://initd.org/psycopg/docs/install.html#binary-install-from-pypi)
    -   mod_wsgi-4.5.15 from [source code](https://modwsgi.readthedocs.io/en/develop/user-guides/quick-installation-guide.html#unpacking-the-source-code) or [APT (Debian)](https://packages.debian.org/ru/sid/httpd/libapache2-mod-wsgi-py3)
    -   geojson from [pip](https://pypi.python.org/pypi/geojson/#installation)
    
For example installation from Advanced Package Tool for Debian:

    sudo sh -c 'echo "deb http://apt.postgresql.org/pub/repos/apt xenial-pgdg main" >> /etc/apt/sources.list'
    wget --quiet -O - http://apt.postgresql.org/pub/repos/apt/ACCC4CF8.asc | sudo apt-key add -
    sudo apt-get update
    sudo apt-get install python3 python3-dev apache2 apache2-dev python3-pip libapache2-mod-wsgi-py3 postgresql-contrib-9.5
    sudo pip3 install psycopg2
    sudo pip3 install geojson

## Deploying ##

Firstly copy project in work directory:

    cd /path/to/work/directory/
    wget https://gitlab.com/simpo-project/simpo_webapp/repository/archive.zip?ref=master
    unzip archive.zip?ref=master

Or you can use git:

    cd /path/to/work/directory/
    git clone https://gitlab.com/simpo-project/simpo_webapp.git

### Configure Apache ###

In scripts folder you can find 010-webapp.conf. It is an example of configuration Apache file.

Add to config apache file in /etc/apache2/apache.config (Ubuntu)

    <IfModule dir_module>
	    DirectoryIndex index.shtml
    </IfModule>

### Create Database ###

For creating databases you must use script:

    sudo -u postgres psql -f scripts/create.db

