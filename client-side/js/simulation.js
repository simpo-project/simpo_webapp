

var ASimulationObj = function() {
}

var SimulationObj = null;

ASimulationObj.prototype.create = function() {
    this.worker = new Worker('js/sim_worker.js');

    this.worker.addEventListener('message', this.worker_callback, false);

    this.worker.postMessage('start ');
}

ASimulationObj.prototype.worker_callback = function(e) {
  console.log(e.data);
}

/*================================================================================*/

function simulationStart(){
    if(SimulationObj != null) {
        throw "Повторный запуск"
    }

    SimulationObj = new ASimulationObj();
    
    var maxTimeForSimulation = prompt("Set the maximum time for searching in ours", "12") * 60;  // Max time in minutes
    var timeStep = prompt("Set the time step in minutes","15");
    var averageSpeed = prompt("Set the average speed of sufferer in km / hr","3");

    SimulationObj.create(maxTimeForSimulation, timeStep, averageSpeed);
}


