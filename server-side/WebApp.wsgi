#!/usr/bin/python3
# -*- coding: utf-8 -*-
import json
import sys
from urllib.parse import parse_qs

from DbHandler import Db
from SessionHandler import Sessions

db = Db()
sessions = Sessions(db)

"""     some methods and data for handling     """


def get_parameters_from_post_query(environ):
    """
    Method replace 5 strings of code for extracting parameters in POST methods
    :param environ: Environment variables dictionary
    :return: Dictionary of parameters
    """
    try:
        request_body_size = int(environ.get('CONTENT_LENGTH', 0))
    except ValueError:
        request_body_size = 0
    request_body = environ['wsgi.input'].read(request_body_size).decode('utf-8')
    return parse_qs(request_body)


def get_json_from_post_query(environ):
    """
    Method replace 5 strings of code for extracting json in POST methods
    :param environ: Environment variables dictionary
    :return: JSON from query
    """
    try:
        request_body_size = int(environ['CONTENT_LENGTH'])
    except ValueError:
        request_body_size = 0
    request_body = environ['wsgi.input'].read(request_body_size).decode('utf-8')
    return json.loads(request_body)


"""     methods for HANDLE queries     """


def org_get(environ, start_response, status=None):
    """
    Handler for /app/org GET method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: Id of organisation or error
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('name'):
            raise Exception
        org_id = db.get_id_org_by_name_org(parameters['name'][0])
        if not org_id:
            raise AttributeError
        return ok_text(environ, start_response, str(org_id))
    except AttributeError:
        return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def list_get(environ, start_response, status=None):
    """
    Handler for /list request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: List in JSON or error
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('start'):
            raise AttributeError
        if not parameters.get('count'):
            raise AttributeError
        if parameters.get('status') and parameters['status'][0] == 'active':
            operations = db.get_list_active(int(parameters['start'][0]), int(parameters['count'][0]))
        else:
            operations = db.get_list(int(parameters['start'][0]), int(parameters['count'][0]))
        response_body = json.dumps(operations)
        return ok_json(environ, start_response, response_body)
    except AttributeError:
        return not_found(environ, start_response)
    except:
        return internal_server_error(environ, start_response)


def sar_info_get(environ, start_response, status=None):
    """
    Handler for /info?id=NUMBER request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: Info in JSON or error
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('id'):
            raise Exception
        operation = db.get_info(int(parameters['id'][0]))
        if status == 'unauthorized':
            operation['private_info'] = ''
            operation['secondary_info'] = ''
        response_body = json.dumps(operation)
        return ok_json(environ, start_response, response_body)
    except AttributeError:
        return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def sar_info_post(environ, start_response, status=None):
    """
    Handler for /app/info POST request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK or 500 error
    """
    try:
        params = get_parameters_from_post_query(environ)
        if params.get('descr') and params.get('org') and params.get('lat') and params.get('long'):
            db.add_short_info(params['descr'][0], int(params['org'][0]), float(params['lat'][0]),
                              float(params['long'][0]))
            return ok_text(environ, start_response)
        else:
            raise Exception
    except Exception:
        return internal_server_error(environ, start_response)


def sar_info_put(environ, start_response, status=None):
    """
    Handler for /app/info PUT request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK or 500 error
    """
    try:
        params = get_parameters_from_post_query(environ)
        if params.get('id') and params.get('descr') and params.get('date') and params.get('status') and params.get(
                'ginfo') and params.get('pinfo') and params.get('sinfo') and params.get('lat') and params.get('long'):
            db.update_info(int(params['id'][0]), params['descr'][0], params['date'][0], params['status'][0],
                           params['ginfo'][0], params['pinfo'][0], params['sinfo'][0], float(params['lat'][0]),
                           float(params['long'][0]))
            return ok_text(environ, start_response)
        else:
            raise Exception
    except Exception:
        return internal_server_error(environ, start_response)


def reminders_get(environ, start_response, status=None):
    """
    Handler for /app/reminders GET request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: list of reminders for id of SAR operation
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('id'):
            raise Exception
        list_reminders = db.get_reminders(int(parameters['id'][0]))
        response_body = json.dumps(list_reminders)
        return ok_json(environ, start_response, response_body)
    except Exception:
        return internal_server_error(environ, start_response)


def reminders_post(environ, start_response, status=None):
    """
    Handler for /app/reminders POST request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: Status 200 OK or 500 Error
    """
    try:
        params = get_parameters_from_post_query(environ)
        if params.get('hours') and params.get('id') and params.get('descr'):
            db.add_reminder_alarm(int(params['id'][0]), int(params['hours'][0]), params['descr'][0])
            return ok_text(environ, start_response)
        elif params.get('active') and params.get('create_date') and params.get('id') and params.get('descr'):
            if params.get('alarm'):
                db.add_reminder(int(params['id'][0]), params['create_date'][0], params['alarm'][0], params['descr'][0],
                                params['active'][0])
            else:
                db.add_reminder(int(params['id'][0]), params['create_date'][0], None, params['descr'][0],
                                params['active'][0])
            return ok_text(environ, start_response)
        elif params.get('id') and params.get('descr'):
            db.add_reminder_no_alarm(int(params['id'][0]), params['descr'][0])
            return ok_text(environ, start_response)
        else:
            raise Exception
    except Exception:
        return internal_server_error(environ, start_response)


def reminders_put(environ, start_response, status=None):
    """
    Handler for /app/reminders PUT request
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: Status 200 OK or 500 Error
    """
    try:
        params = get_parameters_from_post_query(environ)
        if params.get('id') and params.get('create_date') and params.get('alarm') and params.get(
                'descr') and params.get('active'):
            db.update_reminder(int(params['id'][0]), params['create_date'][0], params['alarm'][0], params['descr'][0],
                               params['active'][0])
            return ok_text(environ, start_response)
        elif params.get('id'):
            db.reset_reminder(int(params['id'][0]))
            return ok_text(environ, start_response)
        else:
            raise Exception
    except Exception:
        return internal_server_error(environ, start_response)


def login_get(environ, start_response, status=None):
    """
    Handler for /app/login GET method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK with cookie 'user'
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('user'):
            raise Exception
        if not db.check_user(parameters['user'][0]):
            raise AttributeError
        # if will be using pbkdf2 for auth
        # salt = sessions.create_salt_for_hash1(environ['REMOTE_ADDR'], parameters['user'][0])
        cookie = sessions.create_user_cookie(parameters['user'][0])
        return ok_text(environ, start_response, cookie=cookie)
    except AttributeError:
        return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def login_post(environ, start_response, status=None):
    """
    Handler for /app/login POST method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK with username and cookie 'uid'
    """
    try:
        params = get_parameters_from_post_query(environ)
        if not params.get('user') or not params.get('password'):
            raise Exception
        else:
            cookie, username = sessions.create_session(params['user'][0], params['password'][0], environ['REMOTE_ADDR'])
            return ok_text(environ, start_response, username, cookie=cookie)
    except AttributeError:
        return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def logout(environ, start_response, status=None):
    """
    Handler for /app/logout GET method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK with empty cookie 'user' and 'uid'
    """
    try:
        cookie = sessions.drop_session(environ['HTTP_COOKIE'])
        return ok_text(environ, start_response, cookie=cookie)
    except Exception:
        return internal_server_error(environ, start_response)


def check_permissions(environ, start_response, status=None):
    """
    Handler for getting permissions of user
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK with status as text
    """
    return ok_text(environ, start_response, status)


def geo_data_get(environ, start_response, status=None):
    """
    Handler for /app/geodata GET method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: Set of JSON with geo data
    """
    try:
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('id'):
            raise Exception
        if not parameters.get('layer'):
            raise Exception
        if status == 'unauthorized' and parameters['layer'][0] == 'version':
            return forbidden(environ, start_response)
        elif status == 'sar_member' and parameters['layer'][0] == 'version':
            return forbidden(environ, start_response)
        returned_data = db.get_geo_data(int(parameters['id'][0]), parameters['layer'][0])
        response_body = json.dumps(returned_data)
        return ok_json(environ, start_response, response_body)
    except AttributeError:
        return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def geo_data_post(environ, start_response, status=None):
    """
    Handler for /app/geodata POST method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK or 500 error
    """
    try:
        data = get_json_from_post_query(environ)
        parameters = parse_qs(environ['QUERY_STRING'])
        if not parameters.get('id'):
            raise Exception
        if not parameters.get('layer'):
            raise Exception
        db.set_geo_data_to_db(int(parameters['id'][0]), parameters['layer'][0], data)
        return ok_text(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


def track_post(environ, start_response, status=None):
    """
    Handler for /app/track POST method
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param status: Role of user of this query
    :return: 200 OK or 500 error
    """
    try:
        data = get_json_from_post_query(environ)
        db.set_track_to_db(data)
        return ok_text(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


"""     methods for RESPONSE     """


def ok_json(environ, start_response, response_body, cookie=None):
    """
    Retuning json object with status OK
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param response_body: json object
    :param cookie: cookie header if it needs
    :return: 200 OK HTTP response with json object
    """
    response_body = response_body.encode('utf-8')
    status = '200 OK'
    if cookie:
        response_headers = [cookie, ('Content-Type', 'application/json'),
                            ('Content-Length', str(len(response_body)))]
    else:
        response_headers = [('Content-Type', 'application/json'),
                            ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)
    return [response_body]


def ok_text(environ, start_response, response_body='', cookie=None):
    """
    Retuning text with status OK
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :param response_body: returned text
    :param cookie: cookie header if it needs
    :return: 200 OK HTTP response with text
    """
    response_body = response_body.encode('utf-8')
    status = '200 OK'
    if cookie:
        response_headers = [cookie, ('Content-Type', 'text/plain'),
                            ('Content-Length', str(len(response_body)))]
    else:
        response_headers = [('Content-Type', 'text/plain'),
                            ('Content-Length', str(len(response_body)))]
    start_response(status, response_headers)
    return [response_body]


def not_found(environ, start_response):
    """
    Retuning response with not found error
    :return: 404 Not Found HTTP response
    """
    status = '404 Not Found'
    response_headers = [('Content-type', 'text/plain')]
    start_response(status, response_headers, sys.exc_info())
    return ['Error'.encode('utf-8')]


def forbidden(environ, start_response):
    """
    Retuning response with forbidden error
    :return: 403 Forbidden HTTP response
    """
    status = '403 Forbidden'
    response_headers = [('Content-type', 'text/plain')]
    start_response(status, response_headers, sys.exc_info())
    return ['Error'.encode('utf-8')]


def internal_server_error(environ, start_response):
    """
    Retuning response with internal server error
    :return: 500 Internal Server Error HTTP response
    """
    status = '500 Internal Server Error'
    response_headers = [('Content-type', 'text/plain')]
    start_response(status, response_headers, sys.exc_info())
    return ['Error'.encode('utf-8')]


"""     dictionary of ALIASES and its called functions     """
urls = {
    '/app/loginGET': login_get,
    '/app/loginPOST': login_post,
    '/app/logoutGET': logout,
    '/app/checkGET': check_permissions,
    '/app/orgGET': org_get,
    '/app/listGET': list_get,
    '/app/infoGET': sar_info_get,
    '/app/infoPOST': sar_info_post,
    '/app/infoPUT': sar_info_put,
    '/app/remindersGET': reminders_get,
    '/app/remindersPOST': reminders_post,
    '/app/remindersPUT': reminders_put,
    '/app/geodataGET': geo_data_get,
    '/app/geodataPOST': geo_data_post,
    '/app/trackPOST': track_post,
}

"""     dictionary of PERMISSIONS and its called functions     """
permissions = {
    # only view
    'unauthorized': [login_get, login_post, check_permissions,
                     list_get, sar_info_get,
                     geo_data_get],

    # add track
    'sar_member': [logout, check_permissions,
                   list_get, sar_info_get,
                   geo_data_get, track_post],

    # edit/create SAR op
    'sar_manager': [logout,  check_permissions,
                    org_get, list_get, sar_info_get, sar_info_post, sar_info_put,
                    reminders_get, reminders_post, reminders_put,
                    geo_data_get, geo_data_post, track_post],

    # role management w/o viewing
    # !!! this role hasn't sense w/o admin tools !!!
    # 'admin': [logout, test_get],

    # temporary role for all permissions
    # 'all': [urls[i] for i in urls.keys()]
}

"""     point of ENTRY     """


def application(environ, start_response):
    """
    Point of entry of web application
    :param environ: Environment variables dictionary
    :param start_response: Request handler
    :return: Iterated object with the body of the response
    """
    try:
        path = environ.get('SCRIPT_NAME') + environ.get('REQUEST_METHOD')
        if 'HTTP_COOKIE' in environ:
            current_permission = sessions.check_permission_by_cookie(environ['HTTP_COOKIE'], environ['REMOTE_ADDR'])
        else:
            current_permission = 'unauthorized'
        # temporary all permissions for debug and testing
        # current_permission = 'all'
        if urls.get(path):
            # print('access to {0} with permission: {1}'.format(path, current_permission))
            if urls[path] in permissions[current_permission]:
                return urls[path](environ, start_response, current_permission)
            else:
                return forbidden(environ, start_response)
        else:
            return not_found(environ, start_response)
    except Exception:
        return internal_server_error(environ, start_response)


if __name__ == "__main__":
    print(permissions['all'])
