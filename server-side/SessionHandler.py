#!/usr/bin/python3
# -*- coding: utf-8 -*-
import hashlib
import binascii
import time
from http.cookies import SimpleCookie


class Sessions:
    def __init__(self, db):
        self.db = db
        self.permissions_users = {}  # saving there list of logged in users and its status
        self.name_user = {}  # save there name of logged in user

    def __del__(self):
        # for future add saving sessions to file and load it on initialization
        pass

    @staticmethod
    def create_salt_for_hash1(ip, user):
        # this method for pbkdf2 for auth
        b_salt = ''
        if not user:
            raise Exception
        for i in range(len(user) // 2):
            if i % 2:
                b_salt += chr(ord(user[i]) + 1)
                b_salt += chr(ord(user[-i]) + 3)
            else:
                b_salt += chr(ord(user[i]) - 3)
                b_salt += chr(ord(user[-i]) - 1)
        for i in range(len(ip) // 2):
            if i % 2:
                b_salt += chr(ord(ip[i]) + 30)
                b_salt += chr(ord(ip[-i]) + 28)
            else:
                b_salt += chr(ord(ip[i]) + 25)
                b_salt += chr(ord(ip[-i]) + 23)
        hour = time.strftime("%H", time.gmtime(time.time()))
        b_salt += chr(ord(hour[0]) + 50)
        b_salt = chr(ord(hour[1]) + 35) + b_salt
        return b_salt

    @staticmethod
    def check_hash1(ip, user, hash1):
        # this method for pbkdf2 for auth
        dk = hashlib.pbkdf2_hmac('sha512', b'password', b'salt', 1)

    def create_hash2(self, ip, user):
        password = self.db.get_hash_pwd(user)
        hash_pwd = hashlib.sha512(str(password + ip + user).encode('utf-8')).hexdigest()
        return hash_pwd

    @staticmethod
    def create_user_cookie(user):
        cookie = SimpleCookie()
        cookie['user'] = user
        cookie['user']['Path'] = '/'
        cookie['user']['expires'] = ''
        return 'Set-Cookie', cookie['user'].OutputString()

    @staticmethod
    def drop_user_cookie(str_cookie):
        cookie = SimpleCookie(str_cookie)
        cookie['user'] = ''
        cookie['user']['Path'] = '/'
        cookie['user']['expires'] = ''
        return cookie['user'].OutputString()

    def create_session(self, user, password, ip=None, remember=None):
        # there need use check_hash1 after including pbkdf2
        logon_user = self.db.logon_user(user, password)
        if not logon_user:
            raise AttributeError
        self.permissions_users[user] = logon_user['p_role']
        self.name_user[user] = logon_user['p_name']
        cookie = SimpleCookie()
        hash2 = hashlib.sha512(str(password + ip + user).encode('utf-8')).hexdigest()
        cookie['uid'] = hash2
        cookie['uid']['Path'] = '/'
        if remember:
            # expires for two weeks
            cookie['uid']['expires'] = time.strftime("%a, %d-%b-%Y %T GMT", time.gmtime(time.time() + 14 * 24 * 3600))
        else:
            cookie['uid']['expires'] = ''
        return ('Set-Cookie', cookie['uid'].OutputString()), logon_user['p_name']

    def drop_session(self, str_cookie):
        print(self.name_user)
        print(self.permissions_users)
        cookie = SimpleCookie(str_cookie)
        if cookie.get('user') and cookie.get('uid'):
            self.permissions_users.pop(cookie['user'].value)
            self.name_user.pop(cookie['user'].value)
            cookie = SimpleCookie()
            cookie['uid'] = ''
            cookie['uid']['Path'] = '/'
            cookie['uid']['expires'] = ''
            cookie_user = self.drop_user_cookie(str_cookie)
        else:
            raise Exception
        print(self.name_user)
        print(self.permissions_users)
        return 'Set-Cookie', cookie['uid'].OutputString() + '; ' + cookie_user

    def check_permission_by_cookie(self, str_cookie, ip=None):
        cookie = SimpleCookie(str_cookie)
        if cookie.get('user') and cookie.get('uid'):

            hash_pwd = self.create_hash2(ip, cookie['user'].value)
            if cookie['uid'].value == hash_pwd:
                return self.permissions_users[cookie['user'].value]
            else:
                return 'unauthorized'
        else:
            return 'unauthorized'


if __name__ == "__main__":
    dk = hashlib.pbkdf2_hmac('sha512', b'123', b'Yrtt4JHPMPELLb', 1)
    print(binascii.hexlify(dk).decode('utf-8'))

