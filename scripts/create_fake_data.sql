\c rescue_db

insert into sar_orgs (org_name) values ('Extremum'), ('Other Organization');

insert into sar_ops (sar_num, status, short_desc, org_id, general_info, private_info, secondary_info, lat, long) values
       (1, 'active', 'Киришский Велья Ж 1952гр. с 28.09.2016', 1 ,
'ушла из дома вчера, заявка на пульт сегодня в 12 часов
заявитель - сожитель.
Ранее уже терялась год назад в этом же районе, выходила сама.
Была на связи, сейчас со слов заявителя - нет.
Выехали УЗНТ Кириши.

На границе с Чудовским районом Новгородской области, ЦУКС выясняет территориальную принадлежность.

28.09 между 8 и 9 утра ушла в лес за клюквой. В 17:00 собиралась выйти на дорогу. В 18:00 говорила, что ходит по болоту, дороги не слышит. Около 20:00 решила передохнуть и выходить с утра. В 5:00 телефон уже вне зоны действия сети.

Рост 1.55, нормального телосложения, длинные волосы. Одета: косынка, черная куртка, серые вязанные рейтузы, на ногах резиновые сапоги 37 размера. С собой школьный разноцветный рюкзак, белое или желтое ведерко на 3-4 литра.

Ходит медленно, с палочкой (больной позвоночник). Слух, голос в норме. Зрение: видит плохо, очков не носит. '
, 'Некоторая дополнительная закрытая информация', '',  60.076676, 30.526765),

       (2, 'monitoring', 'Выборгский д.Красный Остров Ж 1936гр. с 01.10.2016', 1 , 'д.Красный Остров на Большом Березовом острове, как добираться - не понятно.
Женщина с расстройством памяти, отдыхала на острове с сыном примерно месяц, ушла в лес в 4 дня.
заявитель - второй сын в СПб (Максим Борисович)
По результатам работы Выборгских спасателей.
Опрос родственников и соседей показал, что бабушка ходит очень мало и очень медленно. Перелезть преграду в виде небольшого ствола для неё крайне затруднительно. Сама ни куда не ходит ввиду возраста и сопутствующих умственных состояний. 1. Осмотрена деревня на предмет нахождения бабушки на территории. Были случаи когда она уходила до соседнего дома и там сидела и ждала пока её спасут. (до 2х дней). Ну и просто упасть где ни будь могла. 2. Отработан участок от дома до болота всеми наличествующими средствами ( в том числе квадрациклы местных жителей). Все дорожки осмотрены в радиусе около 1,5 км от дома. '
,
'тут много закрытой информации

12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345

конец закрытой информации
', '', 59.945175, 30.759549),

       (3, 'closed', 'Тихвинский Мелигерское с/п, дер. Клинцы М 1956гр. с 10.09.2016', 1 ,
'прямая заявка
заявитель - сын

Потерявшийся постоянно проживает в г. Тихвин. Вечером пятницы сказал домашним, что собирается по грибы-ягоды, поедет утренним поездом.
В 10 утра субботы прибыл в деревню, соседи видели уходящим в лес, могут показать направление.
в 9 вечера потерявшегося хватились родственники, звонили, но он не отвечал на звонки.
В воскресенье утром (где-то с 6 до 7-30) потерявшийся пытался позвонить сыну, сын не успел ответить, когда перезванивал через двадцать минут - трубку уже не брали.
Потерявшийся со слов сына физически здоров, без хронических заболеваний.
В это место за ягодами-грибами едет не в первый раз. Есть излюбленные места, по одному такому сын прошел вчера. Вчера искал сын своими силами, в какой-то момент провалился в болото по пояс, после чего вернулся домой.
Сегодня "позвонил в МЧС", через какое-то время ему перезвонили и дали наш номер (вероятней всего, ПЧ).

Отправила в полицию писать заявление и пробовать просить пеленг, обзванивать больницы и БРНС.
Обещала перезвонить через час.

Телефон потерявшегося, со слов сына, в сети.

Информация передана в ЦУКС, ЦУКС заявку принял, думает о возможностях.'
,
'тут много закрытой информации

12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345

конец закрытой информации
'
,
'тут много вторичной информации

12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345
12345 12345 12345 12345 12345 12345 12345

конец вторичной информации
', 59.858620, 29.622099);


insert into persons (p_name, p_login, p_hash, p_role) values
      ('Just Member', 'user1', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'sar_member'),
      ('Sar1 Manager', 'user2', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2',  'sar_manager'),
      ('Sar2 Manager', 'user3', '3c9909afec25354d551dae21590bb26e38d53f2173b8d3dc3eee4c047e7ab1c1eb8b85103e3be7ba613b31bb5c9c36214dc9f14a42fd7a2fdb84856bca5c44c2', 'sar_manager');

insert into sar_op_group (sar_id, comment) values
    (1, 'рпср для ПСР 1'),
    (1, 'группа 2 для ПСР 1'),
    (2, 'рпср для ПСР 2'),
    (3, 'рпср 1 для ПСР 3'),
    (2, 'группа 1 для ПСР 2'),
    (2, 'группа 2 для ПСР 2');

insert into group_persons (group_id, person_id) values
     (1, 3),

     (2, 1),
     (2, 2),

     (5, 1),
     (5, 3),

     (6, 2),
     (6, 3),

     (3, 4),

     (4, 3);



/*--------------------------------------------------------------------------------*/
/*                     GEO                                                        */
insert into geo_data
(sar_id, gdata, descr, l_type)
values
(1, '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"GeometryCollection","geometries":[{"type":"Point","coordinates":[30.1,59.55]},{"type":"Point","coordinates":[29.9,59.50]},{"type":"Point","coordinates":[29.9,59.70]},{"type":"Polygon","coordinates": [[ [29.9,59.50], [30.2,59.50], [30.2,59.70], [29.9,59.70], [29.9,59.50] ]]}]},"properties":{"descr": "version 2"}}]}', 'version 2', 'version'),
(2, '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"GeometryCollection","geometries":[{"type":"Point","coordinates":[30.1,59.55]},{"type":"Point","coordinates":[29.9,59.50]},{"type":"Point","coordinates":[29.9,59.70]},{"type":"Polygon","coordinates": [[ [29.9,59.50], [30.2,59.50], [30.2,59.70], [29.9,59.70], [29.9,59.50] ]]}]},"properties":{"descr": "version 2"}}]}', 'version 3', 'version'),
(3, '{"type":"FeatureCollection","features":[{"type":"Feature","geometry":{"type":"GeometryCollection","geometries":[{"type":"Point","coordinates":[30.1,59.55]},{"type":"Point","coordinates":[29.9,59.50]},{"type":"Point","coordinates":[29.9,59.70]},{"type":"Polygon","coordinates": [[ [29.9,59.50], [30.2,59.50], [30.2,59.70], [29.9,59.70], [29.9,59.50] ]]}]},"properties":{"descr": "version 2"}}]}', 'version 4', 'version'),
(1, '{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [30.564276, 60.057670],
      [30.540957, 60.047827],
      [30.543730, 60.024042]
    ]
  },
  "properties": {
    "descr": "LineString in track"
  }
}', 'track of first group', 'track'),
  (1, '{
  "type": "Feature",
  "geometry": {
    "type": "LineString",
    "coordinates": [
      [30.540957, 60.047827],
      [30.543730, 60.024042],
      [30.564276, 60.057670]
    ]
  },
  "properties": {
    "descr": "LineString in track"
  }
}', 'track of second group', 'track'),

(2, '{"type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [30.520090, 60.030502],
                    [30.512291, 60.045099],
                    [30.522300, 60.060670]
                ]
            },
            "properties": {
                "descr": "LineString in track"
            }
        }', 'track for 2 op', 'track'),
  (3,'{
            "type": "Feature",
            "geometry": {
                "type": "LineString",
                "coordinates": [
                    [30.524077, 60.086643],
                    [30.546155, 60.093774],
                    [30.531451, 60.107924]
                ]
            },
            "properties": {
                "descr": "LineString in track"
            }
        }', 'track for 3 op', 'track'),
(1, '{
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [30.569132, 60.102561]
            },
            "properties": {
                "descr": "Point in routes"
            }
        }', 'routes for 1 op', 'route'),
(2, '{
            "type": "Feature",
            "geometry": {
                "type": "Point",
                "coordinates": [30.521557, 60.064999]
            },
            "properties": {
                "descr": "Point in routes"
            }
        }', 'routes for 2 op', 'route'),
(3, '{
          "type": "Feature",
          "geometry": {
            "type": "Point",
            "coordinates": [30.569176,60.025425]
          },
          "properties": {
                "descr": "Point in routes"
            }
  }', 'routes for 3 op', 'route');

insert into geo_data
(gdata, descr, l_type)
values
('{
        "type": "FeatureCollection",
        "features": [
            {
                "type": "Feature",
                "geometry": {
                    "type": "LineString",
                    "coordinates": [
                        [30.510660, 60.060269],
                        [30.519940, 60.060056],
                        [30.520034, 60.059286]
                    ]
                },
                "properties": {
                    "noise": 15,
                    "slow_koef": 2,
                    "descr": "LineString in const"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Polygon",
                    "coordinates": [[
                        [30.515972, 60.049182],
                        [30.585214, 60.037361],
                        [30.561409, 60.005807],
                        [30.507161, 60.017107]
                    ]]
                },
                "properties": {
                    "noise": 10,
                    "slow_koef": 0,
                    "descr": "Polygon in const"
                }
            },
            {
                "type": "Feature",
                "geometry": {
                    "type": "Point",
                    "coordinates": [30.542937, 60.026630]
                },
                "properties": {
                    "descr":  "Point in const"
                }
            },
          {
            "type": "Feature",
            "geometry": {
              "type": "LineString",
              "coordinates": [
                [30.553380, 60.113435],
                [30.637189, 60.065073],
                [30.610862, 60.054039]
              ]
            },
            "properties": {
              "noise": 4,
              "slow_koef": 6,
              "descr": "LineString in const"
            }
          },
          {
            "type": "Feature",
            "geometry": {
              "type": "Polygon",
              "coordinates": [[
                  [30.634015, 60.078748],
                  [30.537735, 60.075295],
                  [30.520902, 60.098391],
                  [30.602062, 60.105702]
                ]]
            },
            "properties": {
              "noise": 0,
              "slow_koef": 0,
              "descr": "Polygon in const"
            }
          },
          {
            "type": "Feature",
            "geometry": {
              "type": "Point",
              "coordinates": [30.581702, 60.098161]
            },
            "properties": {
              "descr": "Point in const"
            }
          }
        ]
    }', 'const geo data', 'const');



/*--------------------------------------------------------------------------------*/
/*                    Reminders                                                   */
    /* add reminders without alarm */
    select add_reminder_no_alarm_by_sar_num(2, '1111 111');
    select add_reminder_no_alarm_by_sar_num(2, '1111 222');
    select add_reminder_no_alarm_by_sar_num(2, '1111 333');

    /* add any type of reminder */
    select add_reminder_by_sar_num(3, '2017-04-12 15:31:20', null::timestamp,  'none alarm', false);
    select add_reminder_by_sar_num(3, '2017-04-12 16:31:20', null::timestamp,  'none alarm', false);
    select add_reminder_by_sar_num(3, '2017-04-12 15:33:20', '2017-04-12 17:31:20', 'alarm', true);
    select add_reminder_by_sar_num(3, '2017-04-12 11:31:20', '2017-04-12 13:31:20', 'expired alarm', false);

    /* add alarms */ 
    select add_reminder_alarm_by_sar_num(1, 1,  'add 1 hour');
    select add_reminder_alarm_by_sar_num(2, 1,  'add 1 hour');
    select add_reminder_alarm_by_sar_num(2, 2,  'add 2 hours');
    select add_reminder_alarm_by_sar_num(2, 25, 'add 25 hours');

    /* reset alarm as finished */
    select reset_reminder_by_id(8);

/*
TESTS:
[1]
    select get_sar_ops_active(0,3);
    select get_sar_ops(2,1);
    select get_sar_ops(1,3);
    select get_sar_ops(0,10);

    select get_info_of_sar_op_by_num(1);
    select get_info_of_sar_op_by_num(2);

    select get_info_of_sar_op_by_num(999);

[2]
    select * from persons;

[3]
    select get_persons_by_sar_op(1);


[4]
    select * from logon_person('user2', '123');
    select * from logon_person('invalid_user', '123');

[5]
    select * from get_reminders_by_sar_num(1);
    select * from get_reminders_by_sar_num(2);
    select * from get_reminders_by_sar_num(3);
*/
